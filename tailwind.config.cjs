/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			colors: {
				orange: '#f39100',
				grey: "#6c6c75",
			}
		},
	},
	plugins: [
		require('@tailwindcss/typography')
	],
}
