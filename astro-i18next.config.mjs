/** @type {import('astro-i18next').AstroI18nextConfig} */
export default {
  defaultLocale: "de",
  locales: ["de", "en"],
  namespaces: [
    "common",
    "index",
    "download",
    "manual",
    "manualTexts",
    "imprint",
    "contact",
    "privacy-policy",
  ],
};
