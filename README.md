# WIP: Website of CaBr2

## Project Structure

Inside of the project, you'll see the following folders and files:

```plain
/
├── public/
│   └── locales/
│   │   └── de/
│   │   └── en/
│   └── favicon.svg
│   └── first_preview.png
│   └── logo.png
│   └── second_preview_1.png
│   └── second_preview_2.png
├── src/
│   ├── components/
│   └── <currently empty>
│   ├── layouts/
│   │   └── Layout.astro
│   └── pages/
│       └── index.astro
└── package.json
└── tailwind.config.cjs
```

This project is based of the Astro framework.
Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro components.

Any static assets, like images, can be placed in the `public/` directory.

If you want to check other languages than german you have to run `yarn astro-i18next generate` before change the language to prevent weird behavior.

## Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                             |
| :---------------------------- | :------------------------------------------------- |
| `yarn install`                | Installs dependencies                              |
| `yarn dev`                    | Starts local dev server at `localhost:3000`        |
| `yarn build`                  | Build your production site to `./dist/`            |
| `yarn preview`                | Preview your build locally, before deploying       |
| `yarn astro ...`              | Run CLI commands like `astro add`, `astro preview` |
| `yarn astro --help`           | Get help using the Astro CLI                       |
| `yarn astro-i18next generate` | Generate pages for other languages                 |
